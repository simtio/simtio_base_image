# Dockerfile template - SIMTIO

Version 1.0

## Architectures
- amd64
- aarch64
- arm

## Dev

* Clone the repository
* Make edits
* `docker build -f Dockerfile.{stretch,buster} -t simtio/base_image:0.x .`
* `docker push simtio/base_image:0.x`

Multiarch :
* `docker buildx build -f Dockerfile.{stretch,buster} -t simtio/base_image:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push`
